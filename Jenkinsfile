#!/usr/bin/env groovy

pipeline {
    // Declare the agent that will run the pipeline
    agent any

    // Declare the tools that will be used in the pipeline
    tools {
        nodejs "node"
    }

    // Declare the stages of the pipeline
    stages {
        stage('Increment version') {
            steps {
                script {
                    // Change to the "app" directory and run the "npm version minor" command to increment the package version
                    dir("app") {
                        sh "npm version minor"

                        // Read the package.json file and extract the version number
                        def packageJson = readJSON file: 'package.json'
                        def packageVersion = packageJson.version
                        echo "${packageVersion}"

                        // Set the PACKAGE_BUILD_VERSION environment variable to the combination of the package version and the Jenkins build number
                        env.PACKAGE_BUILD_VERSION = "${packageVersion}-${env.BUILD_NUMBER}"
                        echo "PACKAGE_BUILD_VERSION=${env.PACKAGE_BUILD_VERSION}"
                    }
                }
            }
        }
        stage('Run tests') {
            steps {
                script {
                    // Change to the "app" directory and run the "npm install" and "npm run test" commands to install dependencies and run tests
                    dir("app") {
                        sh "npm install"
                        sh "npm run test"
                    }
                }
            }
        }
        stage('Build and Push docker image') {
            steps {
                // Use credentials to authenticate with Docker Hub
                withCredentials([usernamePassword(credentialsId: 'devmentat-dockerhub', usernameVariable: 'USER', passwordVariable: 'PWD')]){
                    // Build and push the Docker image using the PACKAGE_BUILD_VERSION environment variable
                    sh "docker build -t devmentat/training:${PACKAGE_BUILD_VERSION} ."
                    sh "echo ${PWD} | docker login -u ${USER} --password-stdin"
                    sh "docker push devmentat/training:${PACKAGE_BUILD_VERSION}"
                }
            }
        }
        stage('Commit version update') {
            steps {
                script {
                    // Use SSH key to authenticate with GitLab
                    sshagent(['devmentat-gitlab-ssh']) {
                      // Set the git user email and name
                      sh 'git config --global user.email "jenkins@devmentat.com"'
                      sh 'git config --global user.name "jenkins"'

                      // Set the git remote URL
                      sh "git remote set-url origin git@gitlab.com:devmentat/node-project.git"

                      // Add and commit the changes
                      sh 'git add .'
                      sh 'git commit -m "ci: version bump"'

                      // Push the changes to the remote repository
                      sh 'git push origin HEAD:refs/heads/main'
                    }
                }
            }
        }
    }
}
