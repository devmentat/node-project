### DevOps Bootcamp Training

#### Learning Cloud & Jenkins

### Stages of the Jenkins Pipeline

This Jenkins pipeline is used to manage the development and deployment of a Node.js project. It consists of four stages:

1. **Increment version**: The version of the package is incremented using the `npm version` command, and the resulting version number is extracted from the `package.json` file. The `PACKAGE_BUILD_VERSION` environment variable is then set to the combination of the package version and the Jenkins build number.

2. **Run tests**: The project's dependencies are installed using `npm install`, and the tests are run using `npm run test`.

3. **Build and Push docker image**: The Docker image is built and pushed to Docker Hub using the `docker build` and `docker push` commands, respectively. Credentials are used to authenticate with Docker Hub.

4. **Commit version update**: An SSH key is used to authenticate with GitLab and push the version update to the remote repository using the `git push` command. The git user email and name are set, and the git remote URL is updated.

#### Soon Kubernetes...